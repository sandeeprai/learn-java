package com.sdp.problems;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/jumping-on-the-clouds/problem
 * <br>
 * Emma is playing a new mobile game that starts with consecutively numbered clouds. 
 * Some of the clouds are thunderheads and others are cumulus. 
 * She can jump on any cumulus cloud having a number that is equal to the 
 * number of the current cloud plus 1 or 2. She must avoid the thunderheads. 
 * Determine the minimum number of jumps it will take Emma to jump from her 
 * starting postion to the last cloud. It is always possible to win the game.
 * For each game, Emma will get an array of clouds numbered 0 if they are safe or 1 if they must be avoided
 * 
 * @author sandeeprai
 *
 */
public class JumpingOnClouds {

    /**
     * @param c an array of binary integers
     * @return the minimum number of jumps required, as an integer
     */
    static int jumpingOnClouds(int[] c) {
        int result = 0;
        int i=0;

        for (;i<c.length-2;i++){
            if (c[i+1] == 1
                || (c[i+1] == 0 
                && c[i+2] == 0)) i++;

            result++;
        }
        
        return i==c.length-1 ? result : result+1;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] c = new int[n];

        String[] cItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int cItem = Integer.parseInt(cItems[i]);
            c[i] = cItem;
        }

        int result = jumpingOnClouds(c);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

