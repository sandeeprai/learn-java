package com.sdp.problems;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/2d-array/problem
 * <br>
 * Given a 6x6 2D Array, calculate the hourglass sum
 * for every hourglass in arr, then print the maximum hourglass sum.
 * @author sandeeprai
 *
 */
public class HourglassSum {

    /**
     * @param arr an array of integers
     * @return an integer, the maximum hourglass sum in the array
     */
    static int hourglassSum(int[][] arr) {
        List<Integer> list = new ArrayList<>();
        for (int i=0; i<4; i++){
            for (int j=0; j<4;j++){
                list.add(arr[i][j]+arr[i][j+1]+arr[i][j+2]
                    +arr[i+1][j+1]
                    +arr[i+2][j]+arr[i+2][j+1]+arr[i+2][j+2]);
            }
        }
        return Collections.max(list);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int[][] arr = new int[6][6];

        for (int i = 0; i < 6; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 6; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }

        int result = hourglassSum(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
