package com.sdp.problems;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * 
 * https://www.hackerrank.com/challenges/counting-valleys/problem
 * @author sandeeprai
 *
 */
public class CountingValleys {

    /**
     * @param n the number of steps Gary takes
     * @param s a string describing his path e.g. UDDDUDUU
     * @return an integer that denotes the number of valleys Gary traversed.
     */
    static int countingValleys(int n, String s) {
        int count = 0;
        int temp = 0;
        for (int i=0; i<s.length(); i++){
            if ('U'==s.charAt(i)){
                temp++;
                if (temp == 0) count++;
            } else {
                temp--;
            }
            
        }
        return count;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int result = countingValleys(n, s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
