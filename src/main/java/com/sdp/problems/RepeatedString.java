package com.sdp.problems;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * https://www.hackerrank.com/challenges/repeated-string/problem
 * <br>
 * Lilah has a string, s, of lowercase English letters that she repeated infinitely many times.
 * Given an integer, n, find and print the number of letter a's 
 * in the first n letters of Lilah's infinite string.
 * 
 * @author sandeeprai
 *
 */
public class RepeatedString {

    /**
     * @param s a string to repeat
     * @param n the number of characters to consider
     * @return an integer representing the number of occurrences of 'a' in 
     * the prefix of length n in the infinitely repeating string.
     */
    static long repeatedString(String s, long n) {
        long length = s.length();
        long repeats = n/length;
        long charsLeft = n%length;
        
        return countAs(s)*repeats
            + countAs(s.substring(0, (int)charsLeft));
    }

    static long countAs(String s) {
        int count = 0;
        while (s.contains("a")){
            count++;
            s = s.replaceFirst("a", "");
        }
        return count;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        String s = scanner.nextLine();

        long n = scanner.nextLong();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        long result = repeatedString(s, n);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

