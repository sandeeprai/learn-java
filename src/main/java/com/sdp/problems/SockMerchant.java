package com.sdp.problems;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * https://www.hackerrank.com/challenges/sock-merchant/problem
 * <br>
 * John works at a clothing store. He has a large pile of socks that 
 * he must pair by color for sale. Given an array of integers representing 
 * the color of each sock, determine how many pairs of socks with matching colors there are.
 *
 * @author sandeeprai
 */
public class SockMerchant {

    /**
     * @param n the number of socks in the pile
     * @param ar the colors of each sock
     * @return an integer representing the number of matching pairs of socks that are available.
     */
    static int sockMerchant(int n, int[] ar) {
        int count = 0;
        List<Integer> list = new ArrayList<>();
        for (int i=0; i<n; i++){
            if (list.contains(ar[i])){
                count++;
                list.remove(Integer.valueOf(ar[i]));
            } else {
                list.add(ar[i]);
            }
        }
        return count;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = sockMerchant(n, ar);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
