package com.sdp.exercises;

/**
 * Find out how autoboxing and implicit widening causes problems when the reverse happens.
 * 
 * @author sandeeprai
 */
public class Autoboxing {

	public static void main(String[] args) {
		Object[] o = new Object[3];//Line 1
		o[0] = 12;//Line 2
		int i = o[0]; //Line 3
		System.out.print(i);

	}

}
