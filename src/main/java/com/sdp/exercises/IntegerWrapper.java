package com.sdp.exercises;

/**
 * Find out if the {@code Integer} class has a {@code floatValue} method.
 * 
 * @author sandeeprai
 */
public class IntegerWrapper{
	public static void main(String[] argv){
		Integer iw = new Integer(2);
		Integer iw2 = new Integer(2);
		System.out.println(iw * iw2);
		System.out.println(iw.floatValue()); //yes, there is a floatValue method in Integer
	}
}