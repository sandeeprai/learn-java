package com.sdp.learn.generics;

//Example of generic class with two type params
//Syntax: class class-name<type-param1, type-param2,..> {..
public class TwoGen<T, V> {

	private T ob1;
	private V ob2;
	
	TwoGen(T ob1, V ob2){
		this.ob1 = ob1;
		this.ob2 = ob2;
	}
	
	T getOb1() {
		return ob1;
	}
	
	V getOb2() {
		return ob2;
	}
	
	String getGenericTypes() {
		return ob1.getClass().getName()
		 + ", " + ob2.getClass().getName();
	}
}
