## Generics

What: Parameterized types

Why: Enables classes (and interfaces and methods) that will work in a type-safe manner with various types, i.e. *generic* classes

- New syntactical element: `<>`
- Type checking at compile time
- Eliminates casting because compiler knows the type
- Caused changes in the core API code including Collections Framework
- Since JDK5