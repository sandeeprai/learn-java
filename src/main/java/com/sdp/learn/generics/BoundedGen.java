package com.sdp.learn.generics;

//When you want more control on the type param
//This example allows only types that extend Number
//This expects only types that extend Number 
//to perform an operation like doubleValue
public class BoundedGen<T extends Number> {

	private T ob;
	
	BoundedGen(T ob){
		this.ob = ob;
	}
	
	Double getValue() {
		//This is not possible if T is not bounded by Number
		return ob.doubleValue();
	}
}
