package com.sdp.learn.generics;

//T is the type parameter
//It will be replaced by a real type 
//when an object of type T is created
public class Gen<T> {

	private T ob;
	
	Gen(T ob){
		this.ob = ob;
	}
	
	T getOb() {
		return ob;
	}
	
	String getGenericType() {
		return ob.getClass().getName();
	}
}
