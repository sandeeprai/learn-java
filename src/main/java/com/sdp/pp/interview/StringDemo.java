package com.sdp.pp.interview;

import java.util.Scanner;

public class StringDemo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String in = scanner.nextLine();
		char search = scanner.nextLine().charAt(0);
		StringBuilder out = new StringBuilder(in);
		
		for (int i=0; i<in.length();i++) {
			if (out.charAt(i)==search) {
				out.deleteCharAt(i);
				System.out.println("Removed from: "+i);
				break;
			}
		}
		System.out.println(out.toString());
		scanner.close();
	}
}
