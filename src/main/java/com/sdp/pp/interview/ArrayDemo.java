package com.sdp.pp.interview;

import java.util.Scanner;

public class ArrayDemo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int size = Integer.parseInt(scanner.nextLine());
		int[] in = new int[size];
		
		for (int i=0; i<size; i++) {
			in[i] = Integer.parseInt(scanner.nextLine());
		}
		
		int max1 = 0;
		int max2 = 0;
		for (int i=0; i<size; i++) {
			int current = in[i];
			if (current>max1) {
				max2 = max1;
				max1 = current;
			} else if (current>max2) {
				max2 = current;
			}
		}
		
		System.out.println("Largest: "+max1);
		System.out.println("Second largest: "+max2);
		System.out.println("Sum: "+(max1+max2));
		scanner.close();
	}
}
