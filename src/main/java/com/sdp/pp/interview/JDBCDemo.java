package com.sdp.pp.interview;

@Repository
public class JDBCDemo {
	
	JdbcTemplate jdbcTemplate;

	@Autowired
	public JDBCDemo(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public Employee getEmployeeDetails(String id) {
		RowMapper rowMapper = new RowMapper(Employee.class);
		MapSqlParameterSource<String, String> params = new MapSqlParameterSource();
		params.put("id", id);
		String query = "Select * from Employee where EmployeeId=:id";
		return jdbcTemplate.queryForObject(query, params, rowMapper);
		
	}
}
