package com.sdp.learn.generics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BoundedGenTest {
	
	@Test
	public void testBoundedGen() {
		BoundedGen<Integer> gen = new BoundedGen<>(2);
		assertEquals(2.0, gen.getValue());

		BoundedGen<Double> gen1 = new BoundedGen<>(2.2);
		assertEquals(2.2, gen1.getValue());

		BoundedGen<Float> gen2 = new BoundedGen<>(2.0f);
		assertEquals(2.0, gen2.getValue());
				
		//Compilation error because String does not extend Number
		//BoundedGen<String> gen3 = new BoundedGen<String>("2.0");
	}

}
