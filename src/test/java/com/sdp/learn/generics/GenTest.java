package com.sdp.learn.generics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class GenTest {
	
	@Test
	public void testGen() {
		Gen<Integer> gen1 = new Gen<>(2);
		assertEquals(2, gen1.getOb());
		assertEquals("java.lang.Integer", gen1.getGenericType());
		
		Gen<Double> gen2 = new Gen<>(2.0);		
		//Compilation error: Type mismatch: cannot convert from Gen<Integer> to Gen<Double>
		//Example of generics enabling type checking at compile time
		//gen2=gen1;

		//Casting is not required because compiler knows the type
		assertEquals(2.0, (Double) gen2.getOb());
		assertEquals(2.0, gen2.getOb());

		assertEquals("java.lang.Double", gen2.getGenericType());
	}

}
