package com.sdp.learn.generics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TwoGenTest {
	
	@Test
	public void testTwoGen() {
		TwoGen<Integer, String> twoGen = new TwoGen<>(2, "Hello");
		assertEquals(2, twoGen.getOb1());
		assertEquals("Hello", twoGen.getOb2());
		assertEquals("java.lang.Integer, java.lang.String", twoGen.getGenericTypes());
	}

}
